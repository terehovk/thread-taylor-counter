#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <memory.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>
#include <limits.h>
#include <syscall.h>

#define ARGS_COUNT 4
#define THREAD_STATUS_NULL 2
#define THREAD_STATUS_BUSY 1
#define THREAD_STATUS_FREE 0

typedef struct _threadData {
    int id;
    int i, j;
    double x;
    FILE *tmpFile;

} threadData;

char *moduleName;
volatile int *statusThreads;

void printError(const char *moduleName, const char *errorMsg, const char *fileName) {
    fprintf(stderr, "%s: %s %s\n", moduleName, errorMsg, fileName ? fileName : "");
}

double getSinTaylorMember(double x, int memberNumber) {
    double result = 1;
    for (int i = 1; i <= memberNumber * 2 + 1; i++) {
        result *= x / i;
    }
    return (memberNumber % 2) ? -result : result;
}

int writeResult(const int arraySize, FILE *tmpFile, FILE *resultFile, const char *resultPath) {
    double *result = alloca(sizeof(double) * arraySize);
    memset(result, 0, sizeof(double) * arraySize);
    char *fullResultPath = alloca(PATH_MAX);
    realpath(resultPath, fullResultPath);

    int pid, i;
    double memberValue;
    rewind(tmpFile);
    errno = 0;
    while (!feof(tmpFile)) {
        if ((fscanf(tmpFile, "%d %d %lf", &pid, &i, &memberValue) == EOF) && (errno != 0)) {
            printError(moduleName, "Error while reading results from temp file:", strerror(errno));
            return 1;
        }
        result[i] += memberValue;
    }

    for (i = 0; i < arraySize; i++) {
        if (fprintf(resultFile, "y[%d]=%.8lf\n", i, result[i]) == -1) {
            printError(moduleName, strerror(errno), fullResultPath);
            return 1;
        };
    }

    if (fclose(resultFile) == -1) {
        printError(moduleName, strerror(errno), fullResultPath);
        return 1;
    }
    if (fclose(tmpFile) == -1) {
        printError(moduleName, "Error closing temp file: ", strerror(errno));
        return 1;
    }
    return 0;
}

void *threadFunction(void *arg) {

    threadData *t = ((threadData *) arg);

    double member = getSinTaylorMember(t->x, t->j);
    long tid = syscall(SYS_gettid);

    if (fprintf(t->tmpFile, "%ld %d %.8lf\n", tid, t->i, member) == -1) {
        printError(moduleName, "Error writing result to temp file", NULL);
        return NULL;
    };
    printf("%ld %d %lf\n", tid, t->i, member);

    statusThreads[t->id] = THREAD_STATUS_FREE;
    while (statusThreads[t->id] != THREAD_STATUS_NULL);
    statusThreads[t->id] = THREAD_STATUS_FREE;
    return NULL;
}

int getFreeThreadId(int count) {
    int i = 0;
    while (statusThreads[i] == THREAD_STATUS_BUSY) {
        i = (++i == count) ? 0 : i;
    }
    return i;
}

int allThreadDone(int count) {
    for (int i = 0; i < count; i++) {
        if (statusThreads[i] != THREAD_STATUS_FREE)
            return 0;
    }
    return 1;
}


int countFunctionValues(const int arraySize, const int taylorMembersCount, const char *resultPath) {
    FILE *result_file, *tmpFile;
    if (!(result_file = fopen(resultPath, "w+"))) {
        printError(moduleName, strerror(errno), resultPath);
        return 1;
    }

    if (!(tmpFile = tmpfile())) {
        printError(moduleName, strerror(errno), NULL);
        fclose(result_file);
        return 1;
    }

    statusThreads = malloc(taylorMembersCount * sizeof(int));
    for (int i = 0; i < taylorMembersCount; i++) {
        statusThreads[i] = THREAD_STATUS_NULL;
    }
    pthread_t *threads = malloc(taylorMembersCount * sizeof(pthread_t));
    memset((void *) threads, 0, taylorMembersCount * sizeof(pthread_t));
    threadData *threadsData = malloc(taylorMembersCount * sizeof(threadData));

    for (int i = 0; i < arraySize; i++) {
        double x = (2 * M_PI * i) / arraySize;
        if (x != 0) {
            x = M_PI - x;
        }
        for (int j = 0; j < taylorMembersCount; j++) {
            int id = getFreeThreadId(taylorMembersCount);
            threadData *t = &threadsData[id];
            t->id = id;
            t->i = i;
            t->j = j;
            t->x = x;
            t->tmpFile = tmpFile;


            if (statusThreads[id] == THREAD_STATUS_FREE) {
                statusThreads[id] = THREAD_STATUS_NULL;
                pthread_join(threads[id], NULL);
            }
            statusThreads[id] = THREAD_STATUS_BUSY;
            pthread_create(&threads[id], NULL, threadFunction, t);
        }
    }

   for (int i =0; i < taylorMembersCount; i++) {
       while (statusThreads[i] != THREAD_STATUS_FREE);
       statusThreads[i] = THREAD_STATUS_NULL;
       pthread_join(threads[i], NULL);
   }
   while (!allThreadDone(taylorMembersCount));

    free(threads);
    free(threadsData);

    return writeResult(arraySize, tmpFile, result_file, resultPath);
}


int main(int argc, char *argv[]) {
    moduleName = basename(argv[0]);
    int arraySize, taylorMembersCount;

    if (argc != ARGS_COUNT) {
        printError(moduleName, "Wrong number of parameters.", NULL);
        return 1;
    }

    if ((arraySize = atoi(argv[1])) < 1) {
        printError(moduleName, "Array size must be positive.", NULL);
        return 1;
    };
    if ((taylorMembersCount = atoi(argv[2])) < 1) {
        printError(moduleName, "Number of taylor members must be positive.", NULL);
        return 1;
    };

    return countFunctionValues(arraySize, taylorMembersCount, argv[3]);
}